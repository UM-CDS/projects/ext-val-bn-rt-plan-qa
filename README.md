**Repository location moved! Please check [https://github.com/MaastrichtU-CDS/projects_ext-val-bn-rt-plan-qa](https://github.com/MaastrichtU-CDS/projects_ext-val-bn-rt-plan-qa)**
# External validation of the Bayesian network for detecting errors in radiotherapy plans

This repository contains the source code used for the external validation of the Bayesian network for detecting errors in radiotherapy plans described here https://doi.org/10.1002/mp.13515